from fastapi import FastAPI, Depends
from joblib import load
import numpy as np

app = FastAPI()

def get_model():
    model = load("model.joblib")
    return model

@app.post("/predict/")
async def predict(features: list, model=Depends(get_model)):
    features = np.array(features).reshape(1, -1)
    prediction = model.predict(features)
    return {"prediction": prediction[0]}
